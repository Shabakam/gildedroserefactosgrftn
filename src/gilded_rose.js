//Constante utile
const EMPTY_QUALITY = 0;
const DEADLINE = 0;
const MAX_QUALITY = 50;
const TEN_DAYS_REMAINING = 10;
const FIVE_DAYS_REMAINING = 5;

class Item {
  constructor(name, sellIn, quality){
    this.name = name;
    this.sellIn = sellIn;
    this.quality = quality;
  }

  cycle(){
    this.updateSellin()
    this.updateQuality()
  }

  checkSellin(){
    if(this.sellIn >= DEADLINE){
      return true
    }
    else{
      return false
    }
  }

  updateQuality(){
    if(this.checkSellin()){
      this.quality--;
    }else{
      this.quality = 0;
    }
  }

  updateSellin(){
    this.sellIn--;
  }

  checkEmptyQuality(){
    if(this.quality >= EMPTY_QUALITY){
      return true
    }
    else{
      return false
    }
  }

  checkMaxQuality(){
    if(this.quality < MAX_QUALITY){
      return true
    }
    else{
      return false
    }
  }
}

class LegendaryItem extends Item{
  constructor(name, sellIn, quality){
    super(name, sellIn, quality);
  }

  updateQuality(){
    //ne fait rien car l'objet est légendaire
  }
  updateSellin(){
    //ne fait rien car l'objet est légendaire
  }


}

class BonifiedItem extends Item{
  constructor(name, sellIn, quality){
    super(name, sellIn, quality);
  }

  updateQuality(){
    if(this.quality>MAX_QUALITY){
      this.quality=MAX_QUALITY;
    }else if(this.sellIn < DEADLINE){
      this.quality = this.quality + 2;
    }else{
      this.quality++;
    }
  }

}

class ConcertItem extends Item{
  constructor(name, sellIn, quality){
    super(name, sellIn, quality);
  }

  updateQuality(){
    if(this.sellIn<0){
      this.quality=0;

    }
    else if(this.sellIn<FIVE_DAYS_REMAINING) {
      this.quality = this.quality + 3;
      if(this.quality>MAX_QUALITY){
        this.quality=MAX_QUALITY;
      }
    }
    else if(this.sellIn<TEN_DAYS_REMAINING){
      this.quality = this.quality + 2;
      if(this.quality>MAX_QUALITY){
        this.quality=MAX_QUALITY;
      }
    }
    else{
      this.quality++;
      if(this.quality>MAX_QUALITY){
        this.quality=MAX_QUALITY;
      }
    }
    
  }

}

class ConjuredItem extends Item{
  constructor(name, sellIn, quality){
    super(name, sellIn, quality);
  }

  updateQuality(){
    if(this.checkSellin()){
      this.quality=this.quality-2;
    }
    else{
      this.quality=0;
    }
  }
}

class Shop {

  
  constructor(items=[]){
    this.items = items;
    
  }
  
  //Modifie La qualité et le temps de livraison restant en fonction du produit 
  cycle() {
    for (let i = 0; i < this.items.length; i++) {
      this.items[i].cycle()
    }

    if(this.items.length === 0){
      throw 'La liste d\'items ne peut pas être vide.';
    }

    return this.items;
  }
}
module.exports = {
  Item,
  Shop,
  LegendaryItem,
  BonifiedItem,
  ConcertItem,
  ConjuredItem
}
