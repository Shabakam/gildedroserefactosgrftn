# Gilded Rose - Enzo Fonteneau, Yoann Singer

Ce projet est réalisé en js avec l'aide de Mocha/Chai.

Nous avons réussi à utiliser Approvals qui permet de créer des snapshots des résultats de différentes valeurs afin de pouvoir déterminer si nos refactos modifient le comportement de l'application.

Afin de lancer les tests unitaires de la nouvelle fonctionnalité, veuillez taper cette commande :
```
npm run test
```

Afin de comparer avec Approvals la snapshot du résultat de Shop attendu avec le résultat obtenu, veuillez taper cette commande :
```
npm run test:approvals
```