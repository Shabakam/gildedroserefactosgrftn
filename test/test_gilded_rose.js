
var {expect} = require('chai');
var {Shop, Item, ConjuredItem} = require('../src/gilded_rose.js');
describe("Gilded Rose", function() {

  it("test conjured item", function() {
    let conjuredItem = new ConjuredItem("foo", 2, 4);
    let result = new ConjuredItem("foo", 1, 2)
    conjuredItem.cycle();
    expect(conjuredItem).to.eql(result);
  });

  it("test conjured item sellIn 0", function() {
    let conjuredItem = new ConjuredItem("foo", 0, 3);
    let result = new ConjuredItem("foo", -1, 0)
    conjuredItem.cycle();
    expect(conjuredItem).to.eql(result);
  });

});
